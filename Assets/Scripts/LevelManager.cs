﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

	public static LevelManager instance;

	private const string BEST_SCORE_VARIABLE = "BestScore";
	private const string SCORE_TEXT = "Score: ";
	private const string BEST_SCORE_TEXT = " Best: ";

	[SerializeField] private TextMeshProUGUI scoreText;
	[SerializeField] private TextMeshProUGUI bestScoreText;
	[SerializeField] private GameObject gameOverCanvas;

	private Ball ball;
	private Camera camera;

	private float minBallXPos;
	private float maxBallXPos;
	private float groundYPos;
	private Vector3 cameraStartPosition;
	private int score = 0;
	private int bestScore;

	private void Awake()
    {
		if(instance == null)
        {
			instance = this;
        }
		if(instance != this)
        {
			Destroy(gameObject);
        }
    }

	private void Start () {
		camera = Camera.main;
		cameraStartPosition = camera.transform.position;
		ball = FindObjectOfType<Ball>();
		minBallXPos = ball.transform.position.x;
		maxBallXPos = camera.orthographicSize * Screen.width / Screen.height;
		groundYPos = ball.transform.position.y;
		bestScore = PlayerPrefs.GetInt(BEST_SCORE_VARIABLE);
	}
	
	private void Update () {
		if(Input.GetKeyDown(KeyCode.Escape))
        {
			Quit();
        }
	}

	public void NewLevel()
	{
		score++;
		scoreText.text = score.ToString();

		//I decided to create new level by moving ball and the camera
		//Moving flag would require manipulating ground tilemap
		float xPosition = Random.Range(minBallXPos, maxBallXPos);
		camera.transform.position = cameraStartPosition + new Vector3(xPosition, 0, 0);
		ball.transform.position = new Vector3(minBallXPos + xPosition, groundYPos, 0);
		ball.ResetBall();
	}

	public void GameOver()
    {
		if (score > bestScore)
        {
			PlayerPrefs.SetInt(BEST_SCORE_VARIABLE, score);
		}
		bestScoreText.text = SCORE_TEXT + score.ToString() + BEST_SCORE_TEXT + bestScore.ToString();
		gameOverCanvas.SetActive(true);
    }

	public void RestartLevel()
    {
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

	public void Quit()
    {
		Application.Quit();
    }
}
