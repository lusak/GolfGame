﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;

public class Ball : MonoBehaviour {

	//Object to keep hierarchy clean
	[SerializeField] private GameObject ballProjectionsParent;
	[SerializeField] private GameObject ballProjectionPrefab;
	[SerializeField] private Collider2D holeCollider;

	[SerializeField] private float velocityLaunchingIncrease = 0.05f;
	[SerializeField] private float levelCompleteVelocityIncrease = 0.01f;

	[SerializeField] private float angleLaunchingIncrese = 0.2f;
	[SerializeField] private float levelCompleteAngleIncrese = 0.04f;

	[SerializeField] private float ballAssumedStopedVelocity = 0.02f;
	[SerializeField] private int numberOfBallProjections = 9;

	//1 degree is around 0,0174 rad. 1 rad is around 57,295 degrees
	private float angleToRadsFactor = 0.0174f;
	private int timesLaunchButtonClicked = 0;
	private float gravityForce;
	private bool launching = false;
	private bool launched = false;
	private bool checkingLevelFailed = false;
	private float xScreenEndPosition;
	private float timeAfterWhichBallIsAssumedLaunched = 0.1f;
	private float timeBetweenProjectionsDrawed = 0.03f;

	private List<GameObject> ballProjections = new List<GameObject>();

	private Rigidbody2D rBody;
	private Collider2D collider;
	private Camera camera;

	void Start() {
		rBody = GetComponent<Rigidbody2D>();
		collider = GetComponent<Collider2D>();
		gravityForce = Mathf.Abs(Physics2D.gravity.y);
		camera = Camera.main;
		xScreenEndPosition = GetScreenEndXPosition();
		InstantiateBallProjections();
	}

	void Update() {

		if (!launched && (Input.GetKeyUp(KeyCode.Space) || CheckIfLastProjectionOutOfScreen()))
		{
			launching = false;
			LaunchBall();
			StartCoroutine(SetToLaunched());
		}

		if (Input.GetKeyDown(KeyCode.Space) && !launched)
		{
			launching = true;
			StartCoroutine(DrawProjection());
		}

		if (Input.GetKey(KeyCode.Space))
		{
			timesLaunchButtonClicked++;
		}

		if (launched && rBody.velocity.magnitude < ballAssumedStopedVelocity)
		{
			if (collider.IsTouching(holeCollider))
			{
				LevelWon();
			}
			//When ball hits the edge of the hole, sometimes it slows down to almost zero, and then...
			//after a fraction of a second it falls into hole. That's why we need this check
			else if (!checkingLevelFailed)
			{
				checkingLevelFailed = true;
				StartCoroutine(MakeSureLevelFailed());
			}
		}
	}

	public void ResetBall()
	{
		launched = false;
		launching = false;
		checkingLevelFailed = false;
		timesLaunchButtonClicked = 0;
		rBody.velocity = Vector2.zero;
		foreach (GameObject ball in ballProjections)
		{
			ball.transform.position = transform.position;
		}
		xScreenEndPosition = GetScreenEndXPosition();
	}

	private bool CheckIfLastProjectionOutOfScreen()
    {
		if(ballProjections.Count>0)
        {
			//Last ball in the list is the ball furthest away from the ball
			if (ballProjections[ballProjections.Count - 1].transform.position.x > xScreenEndPosition)
			{
				return true;
			}
		}
		return false;
    }

	private IEnumerator DrawProjection()
    {
		while (launching)
        {
			Vector2[] points = GetTrajectoryPoints();

			int ballProjectionIndex = 0;
			Vector2 ballPosition = transform.position;

			foreach (Vector2 point in points)
			{
				GameObject ballProjection = ballProjections[ballProjectionIndex];
				ballProjection.transform.position = ballPosition + point;
				ballProjectionIndex++;
			}
			yield return new WaitForSeconds(timeBetweenProjectionsDrawed);
		}
    }

	private Vector2 GetLaunchVector()
    {
		float launchVelocity = velocityLaunchingIncrease * timesLaunchButtonClicked;
		float launchAngle = angleLaunchingIncrese * timesLaunchButtonClicked * angleToRadsFactor;
		float xVelocity = launchVelocity * Mathf.Abs(Mathf.Cos(launchAngle));
		float yVelocity = launchVelocity * Mathf.Abs(Mathf.Sin(launchAngle));
		return new Vector2(xVelocity, yVelocity);
	}

	private float GetScreenEndXPosition()
	{
		float screenWidthInWorldUnits = 2 * camera.orthographicSize * camera.aspect;
		return transform.position.x + screenWidthInWorldUnits;
	}

	private Vector2[] GetTrajectoryPoints()
    {
		Vector2[] points = new Vector2[numberOfBallProjections];

		float launchAngle = angleLaunchingIncrese * timesLaunchButtonClicked * angleToRadsFactor;
		float launchVelocity = velocityLaunchingIncrease * timesLaunchButtonClicked;

		//Time before ball falls on the ground again
		// t = 2 * v * sin(alfa) / g
		float timeOfFlight = 2 * launchVelocity * Mathf.Abs(Mathf.Sin(launchAngle)) / gravityForce;

		for (int i = 1; i <= numberOfBallProjections; i++)
        {
			//x = v * t * cos(alfa) 
            float x = launchVelocity * timeOfFlight * 1/numberOfBallProjections * i * Mathf.Cos(launchAngle);

			//y = v * t * sin(alfa) - g * t * t / 2
            float y = launchVelocity * timeOfFlight * 1 / numberOfBallProjections * i * Mathf.Sin(launchAngle) 
				- gravityForce / 2 * Mathf.Pow(timeOfFlight * 1 / numberOfBallProjections * i, 2);
			points[i - 1] = new Vector2(x, y);
        }

		return points;
    }

	private void InstantiateBallProjections()
	{
		for (int i = 0; i < numberOfBallProjections; i++)
		{
			GameObject ballProjection = Instantiate(ballProjectionPrefab);
			ballProjection.transform.position = transform.position;
			ballProjection.transform.SetParent(ballProjectionsParent.transform);
			ballProjections.Add(ballProjection);
		}
	}

	private void LevelWon()
    {
		StopAllCoroutines();
		velocityLaunchingIncrease += levelCompleteVelocityIncrease;
		angleLaunchingIncrese += levelCompleteAngleIncrese;
		LevelManager.instance.NewLevel();		
    }

	private void LaunchBall()
    {
		rBody.velocity = GetLaunchVector();
	}

	private IEnumerator MakeSureLevelFailed()
	{
		yield return new WaitForSeconds(1f);
		if (!collider.IsTouching(holeCollider))
		{
			LevelManager.instance.GameOver();
		}
	}

	//This coroutine is to make sure that we don't show game over menu right after...
	//players releases space button, but the ball still does not have any velocity
	private IEnumerator SetToLaunched()
    {
		yield return new WaitForSeconds(timeAfterWhichBallIsAssumedLaunched);
		launched = true;
    }
}
