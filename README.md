GolfGame is a simple 2D golf game (you wouldn't guess it yourself, would you? :)
By holding spacebar you increase ball velocity and launch angle. If ball falls into the hole, you score a point and new level is loaded in which ball accelerates faster during the launching phase.

Unity version 2018.2.21f1